from fastapi import Path, Query, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.compus import Computadora as compusModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from fastapi import APIRouter
from services.compus import CompuService
from schemas.compus import Computadora

computadoras_router = APIRouter()


#Endpoint para devolver el texto "Computadoras" desde la ruta raíz 
@computadoras_router.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1> Computadoras </h1>')


# Endpoint para obtener todas las computadoras
@computadoras_router.get('/computadoras/', tags=['computadoras'], response_model=List[Computadora], status_code=200)
def get_all_computadoras() -> List[Computadora]:
    db = Session()
    result = CompuService(db).get_computadoras()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


# Endpoint para obtener una computadora por su ID
@computadoras_router.get('/computadoras/{id}', tags=['computadoras'], response_model=Computadora, status_code=200)
def get_computadora(id: int = Path(ge=1)) -> Computadora:
    db = Session()
    result = CompuService(db).get_computadora(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


# Endpoint para obtener computadoras por marca
@computadoras_router.get('/computadoras', tags=['computadoras'], response_model=List[Computadora], status_code=200)
def get_computadoras_by_marca(marca: str = Query(min_length=2, max_length=20)):
    db = Session()
    result = CompuService(db).get_computadoras_by_marca(marca)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


# Endpoint para crear una nueva computadora
@computadoras_router.post('/computadoras/', tags=['computadoras'], response_model=dict, status_code=200)
def create_computadora(computadora: Computadora) -> dict:
    db = Session()
    new_computadora = compusModel(**computadora.dict())
    db.add(new_computadora)
    db.commit()
    return JSONResponse(content={"message": "Computadora creada exitosamente", "computadora": new_computadora.dict()})


# Endpoint para actualizar una computadora por su ID
@computadoras_router.put("/computadoras/{id}", tags=['computadoras'], response_model=dict, status_code=200)
def update_computadora(id: int, computadora: Computadora) -> dict:
    db = Session()
    result = CompuService(db).get_computadora(id)
    CompuService(db).update_computadora(id, computadora)
    db.commit()
    return JSONResponse(status_code=200, content={'message': "Computadora actualizada exitosamente"})


# Endpoint para eliminar una computadora por su ID
@computadoras_router.delete('/computadoras/{id}', tags=['computadoras'], response_model=dict, status_code=200)
def delete_computadora(id: int) -> dict:
    db = Session()
    result = db.query(compusModel).filter(compusModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    CompuService(db).delete_computadora(id)
    db.commit()
    return JSONResponse(status_code=200, content={'message': "Computadora eliminada exitosamente"})
