from fastapi import Path, Query, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from fastapi import APIRouter
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()
        
#ENDPOINTS --GET--
@movie_router.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1> Hello World </h1>')

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie,status_code=200)
def get_movies(id: int = Path(ge=1, le =2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie()
    if not result:
        return JSONResponse(status_code=404,content={'message:' "No encontrado"})
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)
def get_all_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies', tags=['movies'], response_model= List[Movie],status_code=200)
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)):
    db = Session()
    result = MovieService(db).get_movies_by_category()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

# ENDPOINT --CREAR--
@movie_router.post('/movies', tags=['movies'], response_model= dict,status_code=200)
def create_movie(movie: Movie) -> dict:
    db = Session()
    #utilizamos el modelo y le pasamos la informacion que vamos a registrar
    new_movie = MovieModel(**movie.model_dump())
    #Ahora a la BD le añadimos la Pelicula
    db.add(new_movie)
    #Guardamos los datos
    db.commit()
    return JSONResponse(content={"message": "Se ha registrado la pelicula"})

#ENDPOINT --EDITAR--
@movie_router.put("/movies/{id}", tags=['movies'], response_model= dict,status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    MovieService(db).update_movie(id,movie)
    db.commit()
    return JSONResponse(status_code=200, content={'message': "Se ha modificado la pelicula"})

#ENDPOINT --ELIMINAR--
@movie_router.delete('/movies/{id}', tags=['movies'], response_model= dict,status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={'message': "Se ha eliminado la pelicula"})

