from pydantic import BaseModel, Field
from typing import Optional

class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length=2, max_length=50)
    modelo: str = Field(min_length=2, max_length=50)
    color: str = Field(min_length=2, max_length=20)
    ram: str = Field(min_length=2, max_length=20)
    almacenamiento: str = Field(min_length=2, max_length=20)
    class Config:
        json_schema_extra = {
        "example": {
            "id": 0,
            "marca": "String",
            "modelo": "String",
            "color": 0,
            "ram": 0,
            "almacenamiento": "String"
        }
    }