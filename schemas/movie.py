from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    id: Optional [int]= None
    title: str = Field(min_length = 5, max_length = 50)
    overview: str = Field(min_length = 5, max_length = 50)
    year: int = Field(le=2024)
    rating: float = Field(ge = 5, le = 10)
    category: str = Field(min_length = 5, max_length = 20)

    class Config:
        json_schema_extra = {
        "example": {
            "id": 0,
            "title": "String",
            "overview": "String",
            "year": 0,
            "rating": 0,
            "category": "String"
        }
    }