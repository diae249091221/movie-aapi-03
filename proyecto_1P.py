from fastapi import FastAPI, Body, Path, Query
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List

app = FastAPI()
app.title = "Mi primera chamba con FastApi"
app.version = "0.0.1"

class Movie(BaseModel):
    id: Optional [int]= None
    title: str = Field(min_length = 5, max_length = 50)
    overview: str = Field(min_length = 5, max_length = 50)
    year: int = Field(le=2024)
    rating: float = Field(ge = 5, le = 10)
    category: str = Field(min_length = 5, max_length = 20)

    class Config:
        json_schema_extra = {
        "example": {
            "id": 0,
            "title": "String",
            "overview": "String",
            "year": 0,
            "rating": 0,
            "category": "String"
        }
    }

#Lista de diccionarios de movies donde se almacenan todas las movies con sus atributos.
movies=[
    {
        "id": 1,
        "title": "Avatar",
        "overview": "En un exuberante planeta llamado Pandora viven unos monos azules",
        "year": 2009,
        "rating": 7.8,
        "category": "Accion"
    },
    {
        "id": 2,
        "title": "The Hunger Games on Fire",
        "overview": "Best movie ever",
        "year": 2014,
        "rating": 10.0,
        "category": "Accion"
    }
]


@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1> Hello World </h1>')

@app.get('/movies/{id}', tags=['movies'], response_model=Movie,status_code=200)
def get_movie(id: int = Path(ge=1, le =2000)) -> Movie:
    for item in movies:
        if item["id"] == id:
            return JSONResponse(content=item)
    return JSONResponse(status_code=404,content=[])

@app.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)
def get_all_movies() -> List[Movie]:
    return JSONResponse(content=movies)

@app.get('/movies', tags=['movies'], response_model= List[Movie],status_code=200)
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)):
    filtered_movies = [movie for movie in movies if movie["category"] == category]
    return JSONResponse(content=filtered_movies)


@app.post('/movies', tags=['movies'], response_model= dict,status_code=200)
def create_movie(movie: Movie) -> dict:
    movies.append(movie)
    return JSONResponse(content={"message": "Se ha registrado la pelicula"})

@app.put("/movies/{id}", tags=['movies'], response_model= dict,status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    for item in movies:
        if item["id"] == id:
            item["title"] = movie.title
            item["overview"] = movie.overview
            item["year"] = movie.year
            item["rating"] = movie.rating
            item["category"] = movie.category
            return movies
    return JSONResponse(content={"message": "Se ha modificado la pelicula"})

@app.delete('/movies/{id}', tags=['movies'], response_model= dict,status_code=200)
def delete_movie(id: int) -> dict:
    global movies
    deleted_movie = None
    movies = [movie for movie in movies if movie["id"] != id]
    
    for movie in movies:
        if movie["id"] == id:
            deleted_movie = movie
            break
    
    print("Movies after deletion:")
    print(movies)
    
    if deleted_movie:
        return JSONResponse(content={"message": f"No se encontro una pelicula con id {id}"})
    else:
        return JSONResponse(content={"message": f"La pelicula con id {id} se ha eliminado", "deleted_movie": deleted_movie})

class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length=2, max_length=50)
    modelo: str = Field(min_length=2, max_length=50)
    color: str = Field(min_length=2, max_length=20)
    ram: str = Field(min_length=2, max_length=20)
    almacenamiento: str = Field(min_length=2, max_length=20)

computadoras=[
    {
        "id": 1,
        "marca": "Huawei",
        "modelo": "Matebook 13s",
        "color": "verde plata",
        "ram": "16gb",
        "almacenamiento": "512gb"
    },
    {
        "id": 2,
        "marca": "Huawei",
        "modelo": "Matebook 13s",
        "color": "negro azabache",
        "ram": "16gb",
        "almacenamiento": "1tb"
    },
    {
        "id": 3,
        "marca": "Dell",
        "modelo": "XPS 13",
        "color": "plateado",
        "ram": "8GB",
        "almacenamiento": "512GB SSD"
    },
    {
        "id": 4,
        "marca": "Lenovo",
        "modelo": "ThinkPad X1 Carbon",
        "color": "negro",
        "ram": "16GB",
        "almacenamiento": "256GB SSD"
    },
    {
        "id": 5,
        "marca": "HP",
        "modelo": "Spectre x360",
        "color": "plata",
        "ram": "12GB",
        "almacenamiento": "512GB SSD"
    }
]

#Endpoint para devolver el texto "Computadoras" desde la ruta raíz 
@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1> Computadoras </h1>')


# Endpoint para obtener todas las computadoras
@app.get('/computadoras/', tags=['computadoras'])
def get_all_computadoras():
    return computadoras

# Endpoint para obtener una computadora por su ID
@app.get('/computadoras/{id}', tags=['computadoras'])
def get_computadora(id: int):
    for comp in computadoras:
        if comp["id"] == id:
            return comp
    return {"message": "Computadora no encontrada"}

# Endpoint para obtener computadoras por marca
@app.get('/computadoras/marca/{marca}', tags=['computadoras'])
def get_computadoras_by_marca(marca: str):
    compus_filtradas = [comp for comp in computadoras if comp["marca"] == marca]
    return compus_filtradas

# Endpoint para crear una nueva computadora
@app.post('/computadoras/', tags=['computadoras'])
def create_computadora(computadora: Computadora):
    computadora_dict = computadora.dict()
    computadora_dict["id"] = len(computadoras) + 1
    computadoras.append(computadora_dict)
    return {"message": "Computadora creada exitosamente", "computadora": computadora_dict}

# Endpoint para actualizar una computadora por su ID
@app.put("/computadoras/{id}", tags=['computadoras'])
def update_computadora(id: int, computadora: Computadora):
    for comp in computadoras:
        if comp["id"] == id:
            comp.update(computadora.dict())
            return {"message": "Computadora actualizada exitosamente", "nueva_computadora": comp}
    return {"message": "Computadora no encontrada"}

# Endpoint para eliminar una computadora por su ID
@app.delete('/computadoras/{id}', tags=['computadoras'])
def delete_computadora(id: int):
    global computadoras
    deleted_computadora = None
    computadoras = [comp for comp in computadoras if comp["id"] != id]
    
    for comp in computadoras:
        if comp["id"] == id:
            deleted_computadora = comp
            break
    
    if deleted_computadora:
        return {"message": f"No se encontró computadora con ID {id}"}
    else:
        return {"message": f"Computadora con ID {id} eliminada exitosamente", "computadora_eliminada": deleted_computadora}

codigo = 0
id = 0

class Categoria(BaseModel):
    id: Optional[int] = None
    categoria: str = Field(min_length = 4, max_length = 15)

    class Config:
        json_schema_extra = {
            "example": {
                "categoria": "Nombre de la categoría"
            }
        }

categorias = []

@app.get('/categorias', tags=['Categorias'], response_model=List[Categoria], status_code=200)
def get_all_categorias() -> List[Categoria]:
    return JSONResponse(content=categorias, status_code=200)

@app.post('/categorias/agregar/', tags=['Categorias'], response_model=dict, status_code=200)
def create_categoria(categoria: Categoria) -> dict:
    global id
    id += 1 
    categoria.id = id
    categoria_dict = categoria.dict()
    categorias.append(categoria_dict)
    return JSONResponse(content={"message": "Se ha registrado exitosamente", "id_asignado": categoria.id})

@app.put('/categorias/actualizar/', tags=['Categorias'], response_model=dict, status_code=200)
def update_categoria(id: int, categoria: Categoria):
    for item1 in categorias:
        if item1["id"] == id:
            categoria_a_cambiar = item1["categoria"]
            item1["categoria"] = categoria.categoria
            for item2 in libros:
                if item2["categoria"] == categoria_a_cambiar:
                    item2["categoria"] = categoria.categoria
            return JSONResponse(content={"message": "Se actualizo exitosamente"}, status_code=200)
    return JSONResponse(content={"message":"No se encontro ninguna categoria con ese id"}, status_code=400)

@app.delete('/categorias/eliminar/', tags=['Categorias'], response_model=dict, status_code=200)
def delete_categoria(id: int) -> dict:
    for item in categorias:
        if item["id"] == id:
            categoria_a_eliminar = item["categoria"]
            for item2 in libros:
                if item2["categoria"] == categoria_a_eliminar:
                    return JSONResponse(content={"message":"Tiene un libro registrado con esta categoria."})
            categorias.remove(item)
            return JSONResponse(content={"message": "Se elimino exitosamente"}, status_code=200)
    return JSONResponse(content={"message":"No se encontro ninguna categoria con ese id"}, status_code=400)


class Libro(BaseModel):
	codigo: Optional[int] = None
	titulo: str = Field(min_length=5, max_length=25)
	autor : str = Field(min_length=5, max_length=20)
	año: int = Field(le=2024)
	categoria: str = Field(min_length=5, max_length=15)
	numeroPaginas: int = Field(ge=49, le=1500)

	class Config:
		json_schema_extra = {
			"example": {
				"titulo": "Titulo del libro",
				"autor": "Autor del libro",
				"año": 1234,
				"categoria": "Categoría del Libro",
				"numeroPaginas": 123
			}
		}

libros = []

@app.get('/libros', tags=['Libros'], response_model=List[Libro], status_code=200)
def get_libros() -> List[Libro]:
    return JSONResponse(content=libros, status_code=200)

@app.get('/libros/{codigo}', tags=['Libros'], status_code=200)
def get_libro(codigo: int = Path(ge=1, le=100)):
    for item in libros:
        if item["codigo"] == codigo:
            return JSONResponse(status_code=200, content=item)
    return JSONResponse(content={"message":"No se encontro ningun libro con ese codigo"}, status_code=400)

@app.get('/libros/categoria/', tags=['Libros'], response_model=List[Libro], status_code=200)
def get_libros_by_category(categoria: str = Query(min_length=5, max_length=15)):
    paJSON = [item for item in libros if item["categoria"] == categoria]
    return JSONResponse(content=paJSON, status_code=200)

@app.post('/libros/registrar/', tags=['Libros'], response_model=dict, status_code=200)
def create_libro(libro: Libro) -> dict:
    for item in categorias:
        if item['categoria'] == libro.categoria:
            global codigo
            codigo += 1
            libro.codigo = codigo
            libro_dict = libro.dict()
            libros.append(libro_dict)
            return JSONResponse(content={"message": "Se ha registrado exitosamente", "codigo_asignado": libro.codigo})
    return JSONResponse(content={"message":"Categoria no registrada"}, status_code=400)

@app.delete('/libros/eliminar/', tags=['Libros'], response_model=dict, status_code=200)
def delete_libro(codigo: int) -> dict:
    for item in libros:
        if item["codigo"] == codigo:
            libros.remove(item)
            return JSONResponse(content={"message":"Se ha elimiando exitosamente"}, status_code=200)
    return JSONResponse(content={"message":"No se encontro ningun libro con ese codigo"}, status_code=400)

@app.put('/libros/actualizar/', tags=['Libros'], response_model=dict, status_code=200)
def update_libro(codigo: int, libro: Libro):
    for item1 in libros:
        if item1["codigo"] == codigo:
            for item2 in categorias:
                if item2["categoria"] == libro.categoria:
                    item1["titulo"] = libro.titulo
                    item1["autor"] = libro.autor
                    item1["año"] = libro.año
                    item1["categoria"] = libro.categoria
                    item1["numeroPaginas"] = libro.numeroPaginas
                    return JSONResponse(content={"message":"Se a realizado la modficación exitosamente"}, status_code=200)
            return JSONResponse(content={"message":"No esta registrada esa categoria"}, status_code=400)
    return JSONResponse(content={"message":"No se encontro ningun libro con ese codigo"}, status_code=400)








