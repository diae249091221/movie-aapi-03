from fastapi import FastAPI
from pydantic import BaseModel, Field
from typing import Optional
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
# from routers.movie import movie_router
from routers.user import user_router
from routers.compus import computadoras_router


app = FastAPI()
app.title = "Mi primera chamba con FastApi"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
# app.include_router(movie_router)
app.include_router(user_router)
app.include_router(computadoras_router)

Base.metadata.create_all(bind=engine)

'''
class Movie(BaseModel):
    id: Optional [int]= None
    title: str = Field(min_length = 5, max_length = 50)
    overview: str = Field(min_length = 5, max_length = 50)
    year: int = Field(le=2024)
    rating: float = Field(ge = 5, le = 10)
    category: str = Field(min_length = 5, max_length = 20)

    class Config:
        json_schema_extra = {
        "example": {
            "id": 0,
            "title": "String",
            "overview": "String",
            "year": 0,
            "rating": 0,
            "category": "String"
        }
    }
'''



        

