from models.compus import Computadora as compusModel
from schemas.compus import Computadora
from pydantic import BaseModel

class CompuService():
    def __init__(self, db) -> None:
        self.db = db

    # Endpoint para obtener todas las computadoras
    def get_all_computadoras(self):
        result = self.db.query(compusModel).all()
        return result

    # Endpoint para obtener una computadora por su ID
    def get_computadora(self,id: int):
        result = self.db.query(compusModel).filter(compusModel.id == id).first()
        return result

    # Endpoint para obtener computadoras por marca
    def get_computadoras_by_marca(self,marca: str):
        result = self.db.query(compusModel).filter(compusModel.marca == marca).all()
        return result

    # Endpoint para crear una nueva computadora
    def create_computadora(self, computadora: Computadora):
        new_computadora = compusModel(**computadora.model_dump())
        self.db.add(new_computadora)
        self.db.commit()
        return

    # Endpoint para actualizar una computadora por su ID
    def update_computadora(self, id: int, data: Computadora):
        computadora = self.db.query(BaseModel).filtter(compusModel.id == id).first()
        computadora.marca = data.marca
        computadora.modelo = data.modelo
        computadora.color = data.color
        computadora.ram = data.ram
        computadora.almacenamiento = data.almacenamiento
        self.db.commit()
        return

    # Endpoint para eliminar una computadora por su ID
    def delete_computadora(self, id: int):
        result = self.db.query(compusModel).filter(compusModel.id == id).first()
        self.db.delete(result)
        self.db.commit()
        return